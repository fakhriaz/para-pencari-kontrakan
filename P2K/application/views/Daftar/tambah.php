<div class="container">
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="card">
			  <div class="card-header">
			  	<h2>Form Tambah Data</h2>
			  </div>
			  <div class="card-body">
			  	<div class="alert alert-primary" role="alert">
				  <?php echo validation_errors(); ?>
				</div>
			   <form action="" method="post" enctype="multipart/form-data">
			   		<div class="form-group ">
				    <label for="id">Id</label>
				    <input type="text" name = "id" class="form-control" id="id" value="<?php echo set_value('id'); ?>">
				  </div>
				  <div class="form-group ">
				    <label for="nama">Nama</label>
				    <input type="text" name = "nama" class="form-control" id="nama" value="<?php echo set_value('nama'); ?>">
				  </div>
				  <div class="form-group">
				    <label for="Lokasi">Lokasi</label>
				    <input type="text" name = "lokasi" class="form-control" id="Lokasi" value="<?php echo set_value('lokasi'); ?>">
				  </div>
				  <div class="form-group">
				    <label for="pemilik">Pemilik</label>
				    <input type="text" name = "pemilik" class="form-control" id="pemilik" value="<?php echo set_value('pemilik'); ?>">
				  </div>
				  <div class="form-group">
				    <label for="nohp">Nomor Handphone</label>
				    <input type="text" name = "nohp" class="form-control" id="nohp" value="<?php echo set_value('nohp'); ?>">
				  </div>
				  <div class="form-group">
				    <label for="spesifikasi">Spesifikasi</label>
				    <input type="text" name="spesifikasi" class="form-control" id="spesifikasi" value="<?php echo set_value('spesifikasi');?>">
				  </div>>
				  <button type="submit" name="tambah" class="btn btn-primary float-right">Tambah</button>
			</form>
			  </div>
			</div>			
		</div>
	</div>
</div>